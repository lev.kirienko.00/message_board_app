from django.shortcuts import render

# Create your views here.
from django.views.generic import ListView

from posts.models import Post, User


class HomePageView(ListView):
    model = Post
    template_name = 'home.html'
    context_object_name = 'all_posts_list'


class UsersView(ListView):
    model = User
    template_name = 'users.html'
    context_object_name = 'all_users_list'
