from django.urls import path

from posts.views import HomePageView, UsersView

urlpatterns = [
    path('', HomePageView.as_view(), name='home'),
    path('users', UsersView.as_view(), name='users')
]
